fn main() {
    let mut buf: String = String::new();
    let mut _input: String = String::new();

    std::io::stdin().read_line(&mut buf).unwrap();
    let mut slices = buf.split_ascii_whitespace();

    for _i in 0..slices.next().unwrap().parse::<i32>().unwrap() {
        _input.clear();
        std::io::stdin().read_line(&mut _input).unwrap();
    }

    let res = slices.next().unwrap().parse::<i32>().unwrap();
    println!("{}", res);
}
